(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Class_group_vdf.Utils

let discriminant_size = 128

let discriminant =
  let seed = Bytes.of_string "secret" in
  generate_discriminant ~seed discriminant_size

module D = struct
  let discriminant = discriminant
end

module CG = Class_group_vdf.Class_group.Make (D)

let random_z () =
  let max_size = Unsigned.Size_t.to_int CG.discriminant_size in
  let rand = Bytes.init max_size (fun _ -> char_of_int @@ Random.int 256) in
  Z.of_bits (Bytes.to_string rand)

module GroupProperties = struct
  (** Verify 0_S * g = 0 where 0_S is the zero of the scalar field, 0 is the
      neutral point and g is a random element *)
  let zero_scalar_nullifier_random () =
    let random = CG.random () in
    assert (CG.is_zero (CG.mul random Z.zero))

  (** Verify 0_S * 0 = 0 where 0_S is the zero of the scalar field and 0 is the
      neutral point *)
  let zero_scalar_nullifier_zero () =
    assert (CG.is_zero (CG.mul CG.zero Z.zero))

  (** Verify 0_S * 1 = 0 where 0_S is the 0 of the scalar field, 1 is a fixed
      generator and 0 is the neutral point *)
  let zero_scalar_nullifier_one () = assert (CG.is_zero (CG.mul CG.one Z.zero))

  let multiply_by_one_does_nothing () =
    let g = CG.random () in
    assert (CG.(eq (mul g Z.one) g))

  (** Verify -(-g) = g where g is a random element *)
  let opposite_of_opposite () =
    let random = CG.random () in
    assert (CG.eq (CG.neg (CG.neg random)) random)

  let opposite_of_opposite_using_scalar () =
    let r = CG.random () in
    assert (CG.(eq r (mul r (Z.neg (Z.neg Z.one)))))

  (** Verify -(-0) = 0 where 0 is the neutral point *)
  let opposite_of_zero_is_zero () = assert (CG.eq (CG.neg CG.zero) CG.zero)

  (** Verify -(-0) = 0 where 0 is the neutral point *)
  let opposite_of_opposite_of_zero_is_zero () =
    assert (CG.eq (CG.neg (CG.neg CG.zero)) CG.zero)

  (** Verify -(-0) = 0 where 0 is the neutral point *)
  let opposite_of_opposite_of_one_is_one () =
    assert (CG.eq (CG.neg (CG.neg CG.one)) CG.one)

  (** Verify g1 + (g2 + g3) = (g1 + g2) + g3 where g1, g2 and g3 are random
      elements *)
  let additive_associativity () =
    let g1 = CG.random () in
    let g2 = CG.random () in
    let g3 = CG.random () in
    assert (CG.eq (CG.add (CG.add g1 g2) g3) (CG.add (CG.add g2 g3) g1))

  (** Verify that g + (-g) = 0 *)
  let opposite_existential_property () =
    let g = CG.random () in
    assert (CG.(eq (add g (neg g)) zero))

  (** Verify a (g1 + g2) = a * g1 + a * g2 where a is a scalar, and g1, g2 are
      two random elements *)
  let distributivity () =
    let s = random_z () in
    let g1 = CG.random () in
    let g2 = CG.random () in
    assert (CG.eq (CG.mul (CG.add g1 g2) s) (CG.add (CG.mul g1 s) (CG.mul g2 s)))

  (** Verify g1 + g2 = g2 + g1 where g1, g2 are two random elements *)
  let commutativity () =
    let g1 = CG.random () in
    let g2 = CG.random () in
    assert (CG.eq (CG.add g1 g2) (CG.add g2 g1))

  (** Verify (a + -a) * g = a * g - a * g = 0 *)
  let opposite_equality () =
    let a = random_z () in
    let g = CG.random () in
    assert (CG.(eq (mul g (Z.add a (Z.neg a))) zero)) ;
    assert (CG.(eq zero (add (mul g a) (mul g (Z.neg a))))) ;
    assert (
      CG.(eq (mul g (Z.add a (Z.neg a))) (add (mul g a) (mul g (Z.neg a)))))

  (** a g + b + g = (a + b) g*)
  let additive_associativity_with_scalar () =
    let a = random_z () in
    let b = random_z () in
    let g = CG.random () in
    let left = CG.(add (mul g a) (mul g b)) in
    let right = CG.(mul g (Z.add a b)) in
    assert (CG.(eq left right))

  (** (a * b) g = a (b g) = b (a g) *)
  let multiplication_properties_on_base_field_element () =
    let a = random_z () in
    let b = random_z () in
    let g = CG.random () in
    assert (CG.(eq (mul g (Z.mul a b)) (mul (mul g a) b))) ;
    assert (CG.(eq (mul g (Z.mul a b)) (mul (mul g b) a)))

  (** Verify (-s) * g = s * (-g) *)
  let opposite_of_scalar_is_opposite () =
    let s = random_z () in
    let g = CG.random () in
    let left = CG.mul g (Z.neg s) in
    let right = CG.mul (CG.neg g) s in
    assert (CG.eq left right)

  (** Returns the tests to be used with Alcotest *)
  let get_tests () =
    let open Alcotest in
    [ test_case "zero_scalar_nullifier_one" `Quick zero_scalar_nullifier_one;
      test_case "zero_scalar_nullifier_zero" `Quick zero_scalar_nullifier_zero;
      test_case
        "zero_scalar_nullifier_random"
        `Quick
        zero_scalar_nullifier_random;
      test_case
        "multiply_by_one_does_nothing"
        `Quick
        multiply_by_one_does_nothing;
      test_case "opposite_of_opposite" `Quick opposite_of_opposite;
      test_case
        "opposite_of_opposite_using_scalar"
        `Quick
        opposite_of_opposite_using_scalar;
      test_case "opposite_of_zero_is_zero" `Quick opposite_of_zero_is_zero;
      test_case
        "opposite_of_opposite_of_zero_is_zero"
        `Quick
        opposite_of_opposite_of_zero_is_zero;
      test_case
        "opposite_of_opposite_of_one_is_one"
        `Quick
        opposite_of_opposite_of_one_is_one;
      test_case "opposite_equality" `Quick opposite_equality;
      test_case "distributivity" `Quick distributivity;
      test_case "commutativity" `Quick commutativity;
      test_case
        "opposite_of_scalar_is_opposite"
        `Quick
        opposite_of_scalar_is_opposite;
      test_case
        "opposite_existential_property"
        `Quick
        opposite_existential_property;
      test_case
        "multiplication_properties_on_base_field_element"
        `Quick
        multiplication_properties_on_base_field_element;
      test_case
        "additive_associativity_with_scalar"
        `Quick
        additive_associativity_with_scalar;
      test_case "additive_associativity" `Quick additive_associativity ]
end

let tests = GroupProperties.get_tests ()
