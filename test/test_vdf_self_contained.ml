(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Class_group_vdf.Vdf_self_contained

let discriminant_size = 128

let difficulty = 1000L

let test_positive () =
  let seed = Bytes.of_string "discriminant_test_positive" in
  let discriminant = generate_discriminant ~seed discriminant_size in
  let seed = Bytes.of_string "challenge_test_positive" in
  let challenge = generate_challenge discriminant seed in
  let res, proof = prove discriminant challenge difficulty in
  assert (verify discriminant challenge difficulty res proof)

let test_recursive () =
  let seed = Bytes.of_string "discriminant_test_recursive" in
  let discriminant = generate_discriminant ~seed discriminant_size in
  let seed = Bytes.of_string "challenge_test_recursive" in
  let challenge = generate_challenge discriminant seed in
  let res, proof = prove discriminant challenge difficulty in
  let b1 = verify discriminant challenge difficulty res proof in
  let challenge2 =
    result_to_bytes res |> challenge_of_bytes_opt |> Option.get
  in
  let res2, proof2 = prove discriminant challenge2 difficulty in
  let b2 = verify discriminant challenge2 difficulty res2 proof2 in
  assert (b1 && b2)

let test_negative () =
  let seed = Bytes.of_string "discriminant_test_negative" in
  let discriminant = generate_discriminant ~seed discriminant_size in
  let seed = Bytes.of_string "challenge_test_negative" in
  let challenge = generate_challenge discriminant seed in
  let res, proof = prove discriminant challenge difficulty in
  let fake_res =
    generate_challenge discriminant (Bytes.create 100)
    |> challenge_to_bytes |> result_of_bytes_opt |> Option.get
  in
  let fake_proof =
    generate_challenge discriminant (Bytes.create 100)
    |> challenge_to_bytes |> proof_of_bytes_opt |> Option.get
  in
  let b = verify discriminant challenge difficulty res proof in
  let b_fake_result = verify discriminant challenge difficulty fake_res proof in
  let b_fake_proof = verify discriminant challenge difficulty res fake_proof in
  let b_fake_both =
    verify discriminant challenge difficulty fake_res fake_proof
  in
  assert b ;
  assert (not b_fake_result) ;
  assert (not b_fake_proof) ;
  assert (not b_fake_both)

let tests =
  [ Alcotest.test_case "positive" `Quick test_positive;
    Alcotest.test_case "recursive" `Quick test_recursive;
    Alcotest.test_case "negative" `Quick test_negative ]
