(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Class_group_vdf.Utils

let discriminant_size = 128

let discriminant =
  let seed = Bytes.of_string "secret" in
  generate_discriminant ~seed discriminant_size

module D = struct
  let discriminant = discriminant
end

module CG = Class_group_vdf.Class_group.Make (D)
module Vdf = Class_group_vdf.Vdf.Make (CG)

let difficulty = Unsigned.UInt64.of_int 1000

let test_positive () =
  let challenge = Vdf.Group.random () in
  let res, proof = Vdf.prove challenge difficulty in
  assert (Vdf.verify challenge difficulty res proof)

let test_recursive () =
  let challenge = Vdf.Group.random () in
  let res, proof = Vdf.prove challenge difficulty in
  let b1 = Vdf.verify challenge difficulty res proof in
  let challenge2 = Vdf.Group.(to_bytes res |> of_bytes_exn) in
  let res2, proof2 = Vdf.prove challenge2 difficulty in
  let b2 = Vdf.verify challenge2 difficulty res2 proof2 in
  assert (b1 && b2)

let test_negative () =
  let challenge = Vdf.Group.random () in
  let res, proof = Vdf.prove challenge difficulty in
  let fake_res = Vdf.Group.random () in
  let fake_proof = Vdf.Group.random () in
  let b = Vdf.verify challenge difficulty res proof in
  let b_fake_result = Vdf.verify challenge difficulty fake_res proof in
  let b_fake_proof = Vdf.verify challenge difficulty res fake_proof in
  let b_fake_both = Vdf.verify challenge difficulty fake_res fake_proof in
  assert b ;
  assert (not b_fake_result) ;
  assert (not b_fake_proof) ;
  assert (not b_fake_both)

let tests =
  [ Alcotest.test_case "positive" `Quick test_positive;
    Alcotest.test_case "recursive" `Quick test_recursive;
    Alcotest.test_case "negative" `Quick test_negative ]
