# `class_group_vdf`

OCaml wrapper for Chia's class group based VDF project (https://github.com/Chia-Network/chiavdf)

```
opam switch create ./ 4.14.0 --deps-only --with-test
dune build @runtest
```
