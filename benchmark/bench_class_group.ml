(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
module Random = Random4

let discriminant_size = 128

let discriminant =
  let open Class_group_vdf.Utils in
  let seed = Bytes.of_string "secret" in
  let start_setup = Sys.time () in
  let d = generate_discriminant ~seed discriminant_size in
  let end_setup = Sys.time () in
  Printf.printf
    "\nGenerating discriminant of size = %d in %fs"
    (discriminant_size * 8)
    (end_setup -. start_setup) ;
  d

module D = struct
  let discriminant = discriminant
end

module CG = Class_group_vdf.Class_group.Make (D)

let () =
  let nb = 1000 in
  let times = ref 0.0 in
  for i = 0 to nb - 1 do
    let start_time = Sys.time () in
    let state = Random.State.make [| i |] in
    let _ = CG.random ~state () in
    let end_time = Sys.time () in
    times := !times +. (end_time -. start_time)
  done ;
  Printf.printf
    "\nAverage random form generation time: %fs (sampled %d elements)"
    (!times /. Float.of_int nb)
    nb ;
  Printf.printf "\n"
