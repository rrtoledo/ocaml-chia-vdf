(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Class_group_vdf.Vdf_self_contained

let discriminant_size = 128

let discriminant =
  let seed = Bytes.of_string "secret" in
  let start_setup = Sys.time () in
  let d = generate_discriminant ~seed discriminant_size in
  let end_setup = Sys.time () in
  Printf.printf
    "\nGenerating discriminant of size = %d in %fs"
    (discriminant_size * 8)
    (end_setup -. start_setup) ;
  d

let () =
  let seed = Bytes.of_string "challenge" in
  let start_challenge = Sys.time () in
  let challenge = generate_challenge discriminant seed in
  let end_challenge = Sys.time () in
  let difficulty = 1000000L in
  let start_proof = Sys.time () in
  let res, proof = prove discriminant challenge difficulty in
  let end_proof = Sys.time () in
  let b = verify discriminant challenge difficulty res proof in
  let end_verify = Sys.time () in
  let its =
    (Float.of_string @@ Int64.to_string difficulty) /. (end_proof -. start_proof)
    |> Int.of_float |> Int.to_string
  in
  Printf.printf
    "\n\nVDF self-contained test with difficulty = %s"
    (Int64.to_string difficulty) ;
  Printf.printf "\n   - challenge time: %fs" (end_challenge -. start_challenge) ;
  Printf.printf "\n   - proving time: %fs" (end_proof -. start_proof) ;
  Printf.printf "\n   - verification time: %fs" (end_verify -. end_proof) ;
  Printf.printf "\n   - IPS: %s" its ;
  Printf.printf "\n" ;
  (* Creates another proof starting at the previous output *)
  let difficulty2 = 2000000L in
  let start_proof = Sys.time () in
  let challenge2 =
    result_to_bytes res |> challenge_of_bytes_opt |> Option.get
  in
  let res2, proof2 = prove discriminant challenge2 difficulty2 in
  let end_proof = Sys.time () in
  let b2 = verify discriminant challenge2 difficulty2 res2 proof2 in
  let end_verify = Sys.time () in
  let its2 =
    (Float.of_string @@ Int64.to_string difficulty2)
    /. (end_proof -. start_proof)
    |> Int.of_float |> Int.to_string
  in
  Printf.printf
    "\nVDF self-contained test with difficulty = %s"
    (Int64.to_string difficulty2) ;
  Printf.printf "\n   - proving time: %fs" (end_proof -. start_proof) ;
  Printf.printf "\n   - verification time: %fs" (end_verify -. end_proof) ;
  Printf.printf "\n   - IPS: %s" its2 ;
  Printf.printf "\n" ;
  assert (b && b2)
