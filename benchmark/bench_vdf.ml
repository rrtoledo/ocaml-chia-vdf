(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Class_group_vdf.Utils

let discriminant_size = 128

let discriminant =
  let seed = Bytes.of_string "secret" in
  generate_discriminant ~seed discriminant_size

module D = struct
  let discriminant = discriminant
end

module CG = Class_group_vdf.Class_group.Make (D)
module Vdf = Class_group_vdf.Vdf.Make (CG)

let () =
  (* let challenge_bytes = Bytes.make CG.size_in_bytes (char_of_int 0) in
     Bytes.set_uint8 challenge_bytes 0 8 ; let challenge =
     Vdf.Group.of_bytes_exn challenge_bytes in *)
  let challenge = Vdf.Group.random () in
  let difficulty = Unsigned.UInt64.of_int 1000000 in
  let start_proof = Sys.time () in
  let res, proof = Vdf.prove challenge difficulty in
  let end_proof = Sys.time () in
  let b = Vdf.verify challenge difficulty res proof in
  let end_verify = Sys.time () in
  let its =
    (Float.of_string @@ Unsigned.UInt64.to_string difficulty)
    /. (end_proof -. start_proof)
    |> Int.of_float |> Int.to_string
  in
  Printf.printf
    "\nVDF test with discriminant size = %d and difficulty = %s"
    (discriminant_size * 8)
    (Unsigned.UInt64.to_string difficulty) ;
  Printf.printf "\n   - proving time: %fs" (end_proof -. start_proof) ;
  Printf.printf "\n   - verification time: %fs" (end_verify -. end_proof) ;
  Printf.printf "\n   - IPS: %s" its ;
  Printf.printf "\n" ;
  (* Creates another proof starting at the previous output *)
  let difficulty2 = Unsigned.UInt64.of_int 2000000 in
  let start_proof = Sys.time () in
  let res2, proof2 = Vdf.prove res difficulty2 in
  let end_proof = Sys.time () in
  let b2 = Vdf.verify res difficulty2 res2 proof2 in
  let end_verify = Sys.time () in
  let its2 =
    (Float.of_string @@ Unsigned.UInt64.to_string difficulty2)
    /. (end_proof -. start_proof)
    |> Int.of_float |> Int.to_string
  in
  Printf.printf
    "\nVDF test with discriminant size = %d and difficulty = %s"
    (discriminant_size * 8)
    (Unsigned.UInt64.to_string difficulty2) ;
  Printf.printf "\n   - proving time: %fs" (end_proof -. start_proof) ;
  Printf.printf "\n   - verification time: %fs" (end_verify -. end_proof) ;
  Printf.printf "\n   - IPS: %s" its2 ;
  Printf.printf "\n" ;
  assert (b && b2)
