(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module type Vdf_sig = sig
  module Group : S.GROUP

  (** [prove_vdf discriminant form difficulty] *)
  val prove : Group.t -> Unsigned.UInt64.t -> Group.t * Group.t

  (** [verify_vdf discriminant form difficulty form form] *)
  val verify : Group.t -> Unsigned.UInt64.t -> Group.t -> Group.t -> bool
end

module Stubs = struct
  external prove :
    Bytes.t ->
    Unsigned.Size_t.t ->
    Bytes.t ->
    Unsigned.UInt64.t ->
    Bytes.t ->
    Bytes.t ->
    int = "caml_prove_bytecode_stubs" "caml_prove_stubs"

  external verify :
    Bytes.t ->
    Unsigned.Size_t.t ->
    Bytes.t ->
    Bytes.t ->
    Bytes.t ->
    Unsigned.UInt64.t ->
    int = "caml_verify_bytecode_stubs" "caml_verify_stubs"
end

module Make (CG : Class_group.Class_group_sig) : Vdf_sig = struct
  module Group = CG

  let discriminant_bytes = Utils.Integer.to_bytes CG.discriminant

  let prove challenge difficulty =
    let challenge_bytes = CG.to_bytes challenge in
    let result_bytes = Bytes.create CG.size_in_bytes in
    let proof_bytes = Bytes.create CG.size_in_bytes in
    let status =
      Stubs.prove
        discriminant_bytes
        CG.discriminant_size
        challenge_bytes
        difficulty
        result_bytes
        proof_bytes
    in
    let result = CG.of_bytes_exn result_bytes in
    let proof = CG.of_bytes_exn proof_bytes in
    match status with
    | x when x = 0 -> (result, proof)
    | x when x = 2 -> raise Utils.invalid_group_element
    | _ -> raise Utils.unknown_error

  let verify challenge difficulty result proof =
    let challenge_bytes = CG.to_bytes challenge in
    let result_bytes = CG.to_bytes result in
    let proof_bytes = CG.to_bytes proof in
    let status =
      Stubs.verify
        discriminant_bytes
        CG.discriminant_size
        challenge_bytes
        result_bytes
        proof_bytes
        difficulty
    in
    match status with
    | x when x = 0 -> false
    | x when x = 1 -> true
    | x when x = 2 -> raise Utils.invalid_group_element
    | _ -> raise Utils.unknown_error
end
