(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Integer : sig
  type t

  val size_in_bytes : t -> int

  val of_bytes : Bytes.t -> t

  val to_bytes : t -> Bytes.t
end = struct
  type t = Bytes.t

  let size_in_bytes x = Bytes.length x

  let of_bytes x = x

  let to_bytes x = x
end

module Stubs = struct
  external create_discriminant : Bytes.t -> int -> int -> Bytes.t -> unit
    = "caml_create_discriminant_stubs"
end

module Utils : sig
  val invalid_group_element : exn

  val unknown_error : exn

  (** [generate_discriminant ?seed size] *)
  val generate_discriminant : ?seed:Bytes.t -> int -> Integer.t
end = struct
  let invalid_group_element =
    Invalid_argument "Invalid input: element not in group"

  let unknown_error = Invalid_argument "Unknown error"

  let generate_discriminant ?(seed = Bytes.empty) size_in_bytes =
    let result = Bytes.create size_in_bytes in
    Stubs.create_discriminant seed (Bytes.length seed) size_in_bytes result ;
    Integer.of_bytes result
end

include Utils
