/* MIT License
 * Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libvdf/src/verifier.h"
#include "libvdf/src/prover_slow.h"
#include "libvdf/src/refcode/lzcnt.c"
#include "libvdf/src/Reducer.h"
#include "libvdf/src/alloc.hpp"

int form_size = BQFC_FORM_SIZE;

int deserialisation_error = 2;

using namespace std;

bool check_bytes(unsigned char *discriminant_bytes,
                 size_t discriminant_size_in_bytes, unsigned char *input) {

  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  try {
    // Deserialising to form
    form f_form = DeserializeForm(discriminant, input, form_size);
    return true;
  } catch (std::exception &e) {
    return false;
  }
}

void zero(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
          unsigned char *result) {

  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  // Generating the identity
  form id_form = form::identity(discriminant);

  // Serialising
  std::vector<uint8_t> res = SerializeForm(id_form, discriminant.num_bits());

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
}

void one(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
         unsigned char *result) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  // Generating the generator
  form g_form = form::generator(discriminant);

  // Serialising
  std::vector<uint8_t> res = SerializeForm(g_form, discriminant.num_bits());

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
}

int eq_form(unsigned char *discriminant_bytes,
            size_t discriminant_size_in_bytes, const unsigned char *f1,
            const unsigned char *f2) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form x1_form;
  form x2_form;
  try {
    // Deserialising to forms
    x1_form = DeserializeForm(discriminant, f1, form_size);
    x2_form = DeserializeForm(discriminant, f2, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Checking if equal
  if (x1_form == x2_form) {
    return 1;
  }
  return 0;
}

int neg_form(unsigned char *discriminant_bytes,
             size_t discriminant_size_in_bytes, unsigned char *f,
             unsigned char *result) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form x_form;
  try {
    // Deserialising to form
    x_form = DeserializeForm(discriminant, f, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Inverting
  form res_form = x_form.inverse();

  // Serialising
  std::vector<uint8_t> res = SerializeForm(res_form, discriminant.num_bits());

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
  return 0;
}

int add_form(unsigned char *discriminant_bytes,
             size_t discriminant_size_in_bytes, unsigned char *f1,
             unsigned char *f2, unsigned char *result) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form x1_form;
  form x2_form;
  try {
    // Deserialising to s
    x1_form = DeserializeForm(discriminant, f1, form_size);
    x2_form = DeserializeForm(discriminant, f2, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Multiplying
  form res_form = multiply(x1_form, x2_form);

  // Serialising
  std::vector<uint8_t> res = SerializeForm(res_form, discriminant.num_bits());

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
  return 0;
}

int double_form(unsigned char *discriminant_bytes,
                size_t discriminant_size_in_bytes, unsigned char *f,
                unsigned char *result) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form x_form;
  try {
    // Deserialising to form
    x_form = DeserializeForm(discriminant, f, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Squaring
  form res_form = square(x_form);

  // Serialising
  std::vector<uint8_t> res = SerializeForm(res_form, discriminant.num_bits());

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
  return 0;
}

int mul_form(unsigned char *discriminant_bytes,
             size_t discriminant_size_in_bytes, unsigned char *f,
             unsigned char *exp_bits, int exp_nb_bits, unsigned char *result) {

  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form x_form;
  try {
    // Deserialising to form
    x_form = DeserializeForm(discriminant, f, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Creating buffer form
  memcpy(result, f, sizeof(unsigned char) * form_size);
  form res_form = DeserializeForm(discriminant, result, form_size);

  // Square and multiply
  for (int i = exp_nb_bits - 2; i >= 0; i--) {
    res_form = square(res_form);
    if (exp_bits[i / 8] & (1 << (i % 8)))
      res_form = multiply(res_form, x_form);
  }

  // Serialising
  std::vector<uint8_t> res = SerializeForm(res_form, discriminant.num_bits());

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
  return 0;
}

void create_discriminant(unsigned char *seed, int seed_size,
                         int discriminant_size, unsigned char *buffer) {
  // Converting to C++ types
  vector<uint8_t> seed_bits(seed, seed + seed_size);

  // Generating safe discriminant
  integer res = CreateDiscriminant(seed_bits, discriminant_size * 8);

  // Serialising to unsigned discriminant in bytes
  vector<uint8_t> res_bytes = res.to_bytes();
  unsigned char *res_data = res_bytes.data();
  memcpy(buffer, res_data, sizeof(unsigned char) * discriminant_size);
}

int prove(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
          unsigned char *challenge, uint64_t num_iterations,
          unsigned char *result, unsigned char *proof) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form x_form;
  try {
    // Deserialising the challenge to form
    x_form = DeserializeForm(discriminant, challenge, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Generating result and proof, already serialised and concatenated
  // together.
  vector<unsigned char> res = ProveSlow(discriminant, x_form, num_iterations);

  // Copying result and proof in respective input buffers
  unsigned char *res_data = res.data();
  memcpy(result, res_data, sizeof(unsigned char) * form_size);
  memcpy(proof, &res_data[form_size], sizeof(unsigned char) * form_size);
  return 0;
}

int verify(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
           const unsigned char *challenge, const unsigned char *result,
           const unsigned char *proof, uint64_t num_iterations) {
  // Deserialising the discriminant and multiplying by -1 as it is always
  // negative
  integer discriminant =
      -integer(discriminant_bytes, discriminant_size_in_bytes);

  form challenge_form;
  form result_form;
  form proof_form;
  try {
    // Deserialising to forms
    challenge_form = DeserializeForm(discriminant, challenge, form_size);
    result_form = DeserializeForm(discriminant, result, form_size);
    proof_form = DeserializeForm(discriminant, proof, form_size);
  } catch (std::exception &e) {
    return deserialisation_error;
  }

  // Verifying proof
  bool is_valid;
  VerifyWesolowskiProof(discriminant, challenge_form, result_form, proof_form,
                        num_iterations, is_valid);

  if (is_valid == true) {
    return 1;
  }
  return 0;
}
