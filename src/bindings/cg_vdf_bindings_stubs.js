/* MIT License
 * Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

//Provides: caml_check_bytes_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_check_bytes_stubs(_d, _d_bits, _x) {
    caml_failwith('Not implemented');
    // return wasm_call('_check_bytes', d, integers_int32_of_uint32(d_bits), x) ? 1 : 0;
}

//Provides: caml_zero_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_zero_stubs(_d, _d_bits, _buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_zero', d, integers_int32_of_uint32(d_bits), buffer);
}

//Provides: caml_one_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_one_stubs(_d, _d_bits, _buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_one', d, integers_int32_of_uint32(d_bits), buffer);
    return 0;
}

//Provides: caml_eq_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_eq_stubs(_d, _d_bits, _x, _y) {
    caml_failwith('Not implemented');
    // return wasm_call('_eq', d, integers_int32_of_uint32(d_bits), x, y) ? 1 : 0;
}


//Provides: caml_neg_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_neg_stubs(d, d_bits, x, buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_neg', d, integers_int32_of_uint32(d_bits), x, buffer);
    // return 0;
}

//Provides: caml_add_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_add_stubs(d, d_bits, x, y, buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_add', d, integers_int32_of_uint32(d_bits), x, y, buffer);
    // return 0;
}

//Provides: caml_double_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_double_stubs(d, d_bits, x, buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_double', d, integers_int32_of_uint32(d_bits), x, buffer);
    // return 0;
}

//Provides: caml_mul_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_mul_stubs(d, d_bits, x, exp, exp_bits, buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_mul', d, integers_int32_of_uint32(d_bits), x, exp, exp_bits, buffer);
    // return 0;
}

//Provides: caml_create_discriminant_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
function caml_create_discriminant_stubs(s, s_bits, d_bits, buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_create_discriminant', s, s_bits, d_bits, buffer);
    // return 0;
}

// WARNING. WASM SUPPORT ONLY 32 bit integers

//Provides: caml_prove_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
//Requires: integers_uint64_of_int
function caml_prove_stubs(d, d_bits, x, n, res_buffer, proof_buffer) {
    caml_failwith('Not implemented');
    // wasm_call('_prove', d, integers_int32_of_uint32(d_bits), x, integers_uint64_of_int(n), res_buffer, proof_buffer);
    // return 0;
}

//Provides: caml_verify_stubs
//Requires: caml_failwith
//Requires: wasm_call
//Requires: integers_int32_of_uint32
//Requires: integers_uint64_of_int
function caml_verify_stubs(d, d_bits, x, r, p, n) {
    caml_failwith('Not implemented');
    // return wasm_call('_verify', d, integers_int32_of_uint32(d_bits), x, r, p, integers_uint64_of_int(n)) ? 1 : 0;
}
