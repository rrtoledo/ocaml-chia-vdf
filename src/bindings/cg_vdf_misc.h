/* MIT License
 * Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CG_VDF_MISC
#define CG_VDF_MISC

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool check_bytes(unsigned char *discriminant_bytes,
                 size_t discriminant_size_in_bytes, unsigned char *input);

void zero(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
          unsigned char *result);

void one(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
         unsigned char *result);

int eq_form(unsigned char *discriminant_bytes,
            size_t discriminant_size_in_bytes, const unsigned char *f1,
            const unsigned char *f2);

int neg_form(unsigned char *discriminant_bytes,
             size_t discriminant_size_in_bytes, unsigned char *f,
             unsigned char *result);

int add_form(unsigned char *discriminant_bytes,
             size_t discriminant_size_in_bytes, unsigned char *f1,
             unsigned char *f2, unsigned char *result);

int double_form(unsigned char *discriminant_bytes,
                size_t discriminant_size_in_bytes, unsigned char *f,
                unsigned char *result);

int mul_form(unsigned char *discriminant_bytes,
             size_t discriminant_size_in_bytes, unsigned char *f,
             unsigned char *exp_bits, int exp_nb_bits, unsigned char *result);

void create_discriminant(unsigned char *seed, int seed_size,
                         int discriminant_size, unsigned char *buffer);

int prove(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
          unsigned char *challenge, uint64_t num_iterations,
          unsigned char *result, unsigned char *proof);

int verify(unsigned char *discriminant_bytes, size_t discriminant_size_in_bytes,
           const unsigned char *challenge, const unsigned char *result,
           const unsigned char *proof, uint64_t num_iterations);
#endif
