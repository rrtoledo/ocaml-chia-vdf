/* MIT License
 * Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cg_vdf_misc.h"

#include <assert.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>
#include "ocaml_integers.h"

// From ocaml-ctypes:
// https://github.com/ocamllabs/ocaml-ctypes/blob/9048ac78b885cc3debeeb020c56ea91f459a4d33/src/ctypes/ctypes_primitives.h#L110
#if SIZE_MAX == UINT64_MAX
#define ctypes_size_t_val Uint64_val
#define ctypes_copy_size_t integers_copy_uint64
#else
#error "No suitable OCaml type available for representing size_t values"
#endif

extern "C" {

CAMLprim value caml_check_bytes_stubs(value discriminant_ml,
                                      value discriminant_size_ml,
                                      value input_ml) {
  CAMLparam3(discriminant_ml, discriminant_size_ml, input_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *input = Bytes_val(input_ml);
  bool res = check_bytes(discriminant_bytes, discriminant_size, input);
  CAMLreturn(Val_bool(res));
}

CAMLprim value caml_zero_stubs(value discriminant_ml,
                               value discriminant_size_ml, value buffer_ml) {
  CAMLparam3(discriminant_ml, discriminant_size_ml, buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  zero(discriminant_bytes, discriminant_size, buffer);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_one_stubs(value discriminant_ml, value discriminant_size_ml,
                              value buffer_ml) {
  CAMLparam3(discriminant_ml, discriminant_size_ml, buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  one(discriminant_bytes, discriminant_size, buffer);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_eq_stubs(value discriminant_ml, value discriminant_size_ml,
                             value f1_ml, value f2_ml) {
  CAMLparam4(discriminant_ml, discriminant_size_ml, f1_ml, f2_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *f1 = Bytes_val(f1_ml);
  unsigned char *f2 = Bytes_val(f2_ml);

  int status = eq_form(discriminant_bytes, discriminant_size, f1, f2);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_neg_stubs(value discriminant_ml, value discriminant_size_ml,
                              value form_ml, value buffer_ml) {
  CAMLparam4(discriminant_ml, discriminant_size_ml, form_ml, buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *form = Bytes_val(form_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  int status = neg_form(discriminant_bytes, discriminant_size, form, buffer);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_add_stubs(value discriminant_ml, value discriminant_size_ml,
                              value first_form_ml, value second_form_ml,
                              value buffer_ml) {
  CAMLparam5(discriminant_ml, discriminant_size_ml, first_form_ml,
             second_form_ml, buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *first_form = Bytes_val(first_form_ml);
  unsigned char *second_form = Bytes_val(second_form_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  int status = add_form(discriminant_bytes, discriminant_size, first_form,
                        second_form, buffer);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_double_stubs(value discriminant_ml,
                                 value discriminant_size_ml, value form_ml,
                                 value buffer_ml) {
  CAMLparam4(discriminant_ml, discriminant_size_ml, form_ml, buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *form = Bytes_val(form_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  int status = double_form(discriminant_bytes, discriminant_size, form, buffer);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_mul_stubs(value discriminant_ml, value discriminant_size_ml,
                              value form_ml, value exp_ml, value exp_nb_bits_ml,
                              value buffer_ml) {
  CAMLparam5(discriminant_ml, discriminant_size_ml, form_ml, exp_ml,
             exp_nb_bits_ml);
  CAMLxparam1(buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *form = Bytes_val(form_ml);
  unsigned char *exp = Bytes_val(exp_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  int status = mul_form(discriminant_bytes, discriminant_size, form, exp,
                        Int_val(exp_nb_bits_ml), buffer);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_mul_bytecode_stubs(value *argv, int argn) {
  return caml_mul_stubs(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
}

CAMLprim value caml_create_discriminant_stubs(value seed_ml, value seed_size_ml,
                                              value discriminant_size_ml,
                                              value buffer_ml) {
  CAMLparam4(seed_ml, seed_size_ml, discriminant_size_ml, buffer_ml);
  unsigned char *seed = Bytes_val(seed_ml);
  int seed_size = Int_val(seed_size_ml);
  int discriminant_size = Int_val(discriminant_size_ml);
  unsigned char *buffer = Bytes_val(buffer_ml);
  create_discriminant(seed, seed_size, discriminant_size, buffer);
  CAMLreturn(Val_unit);
}

CAMLprim value caml_prove_stubs(value discriminant_ml,
                                value discriminant_size_ml, value challenge_ml,
                                value num_iterations_ml, value result_buffer_ml,
                                value proof_buffer_ml) {
  CAMLparam5(discriminant_ml, discriminant_size_ml, challenge_ml,
             num_iterations_ml, result_buffer_ml);
  CAMLxparam1(proof_buffer_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);
  unsigned char *challenge = Bytes_val(challenge_ml);
  uint64_t n = Uint64_val(num_iterations_ml);
  unsigned char *result_buffer = Bytes_val(result_buffer_ml);
  unsigned char *proof_buffer = Bytes_val(proof_buffer_ml);
  int status = prove(discriminant_bytes, discriminant_size, challenge, n,
                     result_buffer, proof_buffer);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_prove_bytecode_stubs(value *argv, int argn) {
  assert(argn == 6);
  return caml_prove_stubs(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
}

CAMLprim value caml_verify_stubs(value discriminant_bytes_ml,
                                 value discriminant_size_ml, value challenge_ml,
                                 value result_ml, value proof_ml,
                                 value num_iterations_ml) {
  CAMLparam5(discriminant_bytes_ml, discriminant_size_ml, challenge_ml,
             result_ml, proof_ml);
  CAMLxparam1(num_iterations_ml);
  unsigned char *discriminant_bytes = Bytes_val(discriminant_bytes_ml);
  size_t discriminant_size = ctypes_size_t_val(discriminant_size_ml);

  unsigned char *challenge = Bytes_val(challenge_ml);
  unsigned char *result = Bytes_val(result_ml);
  unsigned char *proof = Bytes_val(proof_ml);
  uint64_t n = Uint64_val(num_iterations_ml);

  // discriminant must be an integer
  int status = verify(discriminant_bytes, discriminant_size, challenge, result,
                      proof, n);
  CAMLreturn(Val_int(status));
}

CAMLprim value caml_verify_bytecode_stubs(value *argv, int argn) {
  assert(argn == 6);
  return caml_verify_stubs(argv[0], argv[1], argv[2], argv[3], argv[4],
                           argv[5]);
}
}
